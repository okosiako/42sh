# 42sh

42sh is next shell-project after [21sh](https://bitbucket.org/Mitriksicilian/uf_7_21_sh). 42sh has some overall improvements and job control(*jobs*, *fg* and *bg*).
It is a command project for 4 - 5 students.

## Installing

Clone this repository recusively:
```
git clone --recursive
```
and
```
make
```

## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Authors

* [Atverdok1](https://bitbucket.org/atverdok1/)
* [Dr-Faust](https://bitbucket.org/Dr-Faust/)
* [Dmytro Kovalchuk](https://bitbucket.org/Mitriksicilian/)
* [Olha Kosiakova](https://bitbucket.org/okosiako/)
* [Palyvoda](https://bitbucket.org/Palyvoda/)


## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=42sh from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to UNIT Factory, for inspiration to do my best.
* to all UNIT Factory students, who shared their knowledge with me and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/img/vi.jpg)

Learn more about this [innovative programming school](https://unit.ua/) in [the heart](https://en.wikipedia.org/wiki/Kiev) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
