/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs_children.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/04 20:47:23 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/27 08:48:21 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 42sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRUCTS_CHILDREN_H
# define STRUCTS_CHILDREN_H

/*
**	This file provides child struct, and functions helpers.
**
**	Should be used:
**	everywhere we're working with children we're interested in.
**
**	Function naming:
**	st_chl_...
*/

# include <stdbool.h>
# include <sys/types.h>

/*
**			Children structure:
**
**	pid			|	Process id
**	pid			|	Process group id
**	number		|	Number of the job
**	command		|	Full path to the command, used only in printing
**	raw_data	|	The input we got from the user
**	state		|	The state of the process
**	is_default	|	Marks the default job
**	is_2default	|	Marks the job, which will be a default job after
**				|		the default one is done with
**	remove		|	True - if we can delete current item
**	print		|	True - if we have updated the message, and should print it
*/

typedef struct			s_child_list
{
	pid_t				pid;
	pid_t				pgid;
	const unsigned		number;
	const char *const	command;
	const char *const	raw_data;
	const char			*state;
	bool				is_default;
	bool				is_2default;
	bool				remove;
	bool				print;
	struct s_child_list	*next;
}						t_child_list;

t_child_list			*st_chl_new(const char *const cmd,
						const char *const raw_data, const char *const state);
t_child_list			*st_chl_find(const pid_t pid_in);
t_child_list			*st_chl_find_nb(const unsigned number);
t_child_list			*st_chl_get_default(void);
void					st_chl_push_back(t_child_list *const child);
void					st_chl_delete(const pid_t pid_in);

#endif
