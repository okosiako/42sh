/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/03 21:41:34 by okosiako          #+#    #+#             */
/*   Updated: 2017/11/05 14:09:15 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 42sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "structs_global.h"
#include "structs_history.h"

/*
** This function looks for element from history by index.
**
** Return value:
**		appropriate history	-	success
**		NULL				-	fail
*/

t_history_list		*st_hst_get_by_index(int index)
{
	t_history_list	*elem;

	elem = g_global->history;
	if (index < 0)
	{
		while (++index && elem)
			elem = elem->prev;
	}
	else if (index > 0)
	{
		while (elem && elem->prev)
			elem = elem->prev;
		while (--index && elem)
			elem = elem->next;
	}
	return (elem);
}
