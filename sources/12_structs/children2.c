/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   children2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/04 21:02:08 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/27 08:48:14 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 42sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "structs_global.h"
#include "structs_children.h"

/*
**	Structures - children
**	This function creates a new child structure. First it generates new
**	job number, and only then creates a structure.
**
**	Return value:
**		pointer to newly created structure.
*/

t_child_list	*st_chl_new(const char *const cmd,
	const char *const raw_data, const char *const state)
{
	t_child_list	*rt;
	unsigned		number;

	number = 0;
	rt = g_global->children;
	while (rt != NULL)
	{
		number = MAX(number, rt->number);
		rt = rt->next;
	}
	number++;
	rt = (t_child_list*)ft_memalloc(sizeof(t_child_list));
	rt->print = true;
	*((unsigned*)&(rt->number)) = number;
	*((char**)&(rt->command)) = ft_strdup(cmd);
	*((char**)&(rt->raw_data)) = ft_strdup(raw_data);
	rt->state = ft_strdup(state);
	return (rt);
}

/*
**	Structures - children
**	This function looks for a child structure with given pid.
**
**	Return value:
**		pointer to the structure with corresponding pid.
**		NULL if not found.
*/

t_child_list	*st_chl_find(const pid_t pid_in)
{
	t_child_list	*rt;

	rt = g_global->children;
	while (rt)
		if (rt->pid == pid_in)
			return (rt);
		else
			rt = rt->next;
	return (NULL);
}

/*
**	Structures - children
**	This function looks for a child structure with given job number.
**
**	Return value:
**		pointer to the structure with corresponding number.
**		NULL if not found.
*/

t_child_list	*st_chl_find_nb(const unsigned number)
{
	t_child_list	*rt;

	rt = g_global->children;
	while (rt)
		if (rt->number == number)
			return (rt);
		else
			rt = rt->next;
	return (NULL);
}

/*
**	Structures - children
**	This function returns default child.
**
**	Return value:
**		pointer to the default child.
**		NULL if there is no default child.
*/

t_child_list	*st_chl_get_default(void)
{
	t_child_list	*result;

	result = g_global->children;
	while (result)
	{
		if (result->is_default)
			return (result);
		result = result->next;
	}
	return (NULL);
}
