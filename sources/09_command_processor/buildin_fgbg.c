/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buildin_fgbg.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 20:43:25 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/28 22:16:33 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 42sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include <signal.h>
#include "structs_global.h"
#include "structs_command.h"
#include "structs_children.h"
#include "command_processor_internal.h"

/*
**	Command processor - buildins.
**	This function retuns proper job number. It either gets it from the argument,
**	or returns the default one.
**
**	Return value:
**		!NULL	-	OK
**		NULL	-	error, with message already printed
*/

static t_child_list	*get_job(const char *const arg)
{
	t_child_list	*result;

	if (arg == NULL || ft_strlen(arg) < 1)
	{
		if ((result = st_chl_get_default()) == NULL)
			err_print("fg", "no default job");
	}
	else
	{
		if ((result = st_chl_find_nb(ft_atoi(arg))) == NULL)
			err_print_long("fg", "no such job", arg);
	}
	return (result);
}

/*
**	Command processor - buildins.
**	This function moves given child group of processes to the foreground.
**	Otherwise it prints an error.
**
**	Return value:
**		true	-	OK
**		false	-	error, with message already printed
*/

bool				cp_buildin_fg(const t_command *const cmd)
{
	int				status;
	t_child_list	*job;
	t_command		job_cmd;

	if ((job = get_job(cmd->args ? cmd->args[1] : NULL)) == NULL)
		return (false);
	ft_putendl(job->command);
	job_cmd.pid = job->pid;
	job_cmd.pgid = job->pgid;
	job_cmd.name = job->command;
	*((const char**)&(job_cmd.raw_data)) = ft_strdup(job->raw_data);
	st_chl_delete(job_cmd.pid);
	status = cp_s4_parent_main(&job_cmd);
	ft_memdel((void**)&(job_cmd.raw_data));
	return (status);
}

/*
**	Command processor - buildins.
**	This function moves given child group of processes to the background,
**	and sends SIGCONT signal to continue those processes.
**	Otherwise it prints an error.
**
**	Return value:
**		true	-	OK
**		false	-	error, with message already printed
*/

bool				cp_buildin_bg(const t_command *const cmd)
{
	t_child_list	*child;

	child = g_global->children;
	while (child && (cmd || !cmd))
	{
		if (!ft_strequ(child->state, "Running"))
		{
			killpg(child->pgid, SIGCONT);
			ft_memdel((void**)&(child->state));
			child->state = ft_strdup("Running");
			child->print = true;
		}
		child = child->next;
	}
	return (true);
}
