/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage4_parent.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 13:32:40 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/28 22:14:38 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 42sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include <signal.h>
#include "service.h"
#include <sys/wait.h>
#include "structs_global.h"
#include "structs_command.h"
#include "structs_children.h"

/*
**	Command processor
**	This function waits for all processes in the given group, and returns
**	the status of the group leader(pid == pgid). If any of the processes is
**	signaled with a SIGSTOP / SIGTSTOP, we add the whole job into background
**	(because the signal proragates to all processes in the
**	group we're waiting for).
**
**	Return value:
**		exit status of the main process in the group
*/

static int	cp_s4_get_leader_exit_status(const pid_t pgid,
			const t_command *const cmd)
{
	t_child_list	*child;
	int				status;
	int				dummy;
	pid_t			pid;

	status = 0;
	while ((pid = waitpid(-(pgid), &dummy, WUNTRACED)) != -1)
	{
		if (pid == pgid)
			status = dummy;
		if (WIFSTOPPED(dummy))
		{
			child = st_chl_new(cmd->name, cmd->raw_data, "Stopped");
			child->pid = cmd->pid;
			child->pgid = cmd->pgid;
			st_chl_push_back(child);
			break ;
		}
	}
	return (status);
}

/*
**	Command processor
**	This function tries to give the controlling terminal to the given process,
**	if it fails, prints an error message.
*/

static void	cp_s4_change_fg_proc(const pid_t pgid)
{
	if (tcsetpgrp(g_global->terminal_fd, pgid) != 0)
		err_print("error setting foreground process",
		"cannot give the terminal to the process");
}

/*
**	Command processor
**	This function sets the command as a foreground process, changes SIGCHLD
**	disposition and waits, returning the exit code after that, if, and only
**	if, this command was not meant to run in background(background command or
**	piped command).
**
**	Return value:
**		true	-	given command exited with code 0, and status 0,
**					or was run background
**		false	-	given command exited with non-zero code
*/

bool		cp_s4_parent_main(const t_command *const cmd)
{
	int						status;
	const struct sigaction	old;
	struct sigaction		new;

	status = 0;
	sigaction(SIGCHLD, NULL, (struct sigaction *)(&old));
	new = old;
	new.sa_handler = SIG_DFL;
	if (sigaction(SIGCHLD, &new, NULL) != 0)
		err_print(ERR_REG_SIGNAL, NULL);
	tcsetattr(g_global->terminal_fd, TCSADRAIN, &(g_global->terminal_origin));
	cp_s4_change_fg_proc(cmd->pgid);
	killpg(cmd->pgid, SIGCONT);
	status = cp_s4_get_leader_exit_status(cmd->pgid, cmd);
	cp_s4_change_fg_proc(g_global->sh_pgid);
	tcsetattr(g_global->terminal_fd, TCSADRAIN, &(g_global->terminal_mine));
	if (sigaction(SIGCHLD, &old, NULL) != 0)
		err_print(ERR_REG_SIGNAL, NULL);
	raise(SIGCHLD);
	return (!WEXITSTATUS(status));
}
