/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buildin_jobs.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 16:10:18 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/27 08:48:36 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 42sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include "ft_printf.h"
#include "structs_global.h"
#include "structs_command.h"
#include "structs_children.h"

/*
**	Command processor - buildins.
**	This function checks that if we have an argument it is either -l or -p.
**	Otherwise it prints an error.
**
**	Return value:
**		true	-	arguments are OK
**		false	-	arguments are not OK
*/

static bool		check_arguments(const char *const *const args)
{
	size_t	ct;

	ct = 0;
	while (args != NULL && args[ct] != NULL)
	{
		if (args[ct][0] == '-'
		&& ((args[ct][1] != 'l' && args[ct][1] != 'p') || args[ct][2] != '\0'))
		{
			err_print(ERR_UNK_OPT, args[ct]);
			ft_putstr("jobs: usage: jobs [-l|-p] [job_num ...]");
			return (false);
		}
		ct++;
	}
	return (true);
}

/*
**	Command processor - buildins.
**	This function works with given arguments, it print info if arguments
**	match the job number, or it prints all jobs if there are no arguments.
*/

static void		print(const bool list, const bool pids, t_child_list *child)
{
	char	def;

	def = ' ';
	def = child->is_default ? '+' : def;
	def = child->is_2default ? '-' : def;
	if (pids)
		ft_printf("%d", child->pid);
	else
	{
		ft_printf("[%2d]%c ", child->number, def);
		if (list)
			ft_printf("%-5d ", child->pid);
		ft_printf("%40s %s (%s)",
			child->state, child->command, child->raw_data);
	}
	ft_putchar('\n');
}

/*
**	Command processor - buildins.
**	This function works with given arguments, it print info if arguments
**	match the job number, or it prints all jobs if there are no arguments.
*/

static void		do_jobs(const bool list, const bool pids,
				const char *const *const args)
{
	size_t			ct;
	t_child_list	*temp;

	ct = 0;
	while (args != NULL && args[ct] != NULL)
	{
		if ((temp = st_chl_find_nb(atoi(args[ct]))) != NULL)
			print(list, pids, temp);
		else
			err_print("no such job", args[ct]);
		ct++;
	}
	if (ct == 0)
	{
		temp = g_global->children;
		while (temp)
		{
			print(list, pids, temp);
			temp = temp->next;
		}
	}
}

/*
**	Command processor - buildins.
**	This function prints all current jobs we have.
**
**	Return value:
**		true	-	command was executed
**		false	-	command was not executed due to errors
*/

bool			cp_buildin_jobs(const t_command *const cmd)
{
	bool	l_arg;
	bool	p_arg;
	size_t	ct;

	if (!check_arguments((const char *const *const)(cmd->args)))
		return (false);
	ct = 1;
	while (cmd->args != NULL
	&& cmd->args[ct] != NULL && cmd->args[ct][0] == '-')
		ct++;
	ct--;
	l_arg = cmd->args[ct] != NULL && cmd->args[ct][1] == 'l';
	p_arg = cmd->args[ct] != NULL && cmd->args[ct][1] == 'p';
	do_jobs(l_arg, p_arg, (const char *const *const)(&(cmd->args[ct + 1])));
	return (true);
}
