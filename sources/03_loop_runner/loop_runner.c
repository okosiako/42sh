/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop_runner.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 21:06:33 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/04 23:12:00 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 42sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "line_reader.h"
#include "line_processor.h"
#include "prompt_builder.h"
#include "structs_global.h"
#include "structs_history.h"
#include "structs_command.h"
#include "command_processor.h"

/*
**	Loop runnner
**	This function prints messages from children, right before
**	printing the prompt.
*/

void		lr_loop_print_chl_messages(void)
{
	char			def;
	bool			space;
	t_child_list	*temp;
	t_child_list	*delete;

	space = true;
	temp = g_global->children;
	while (temp)
	{
		if (temp->print)
		{
			if (space)
				ft_putchar('\n');
			space = false;
			def = temp->is_default ? '+' : ' ';
			def = temp->is_2default ? '-' : def;
			ft_printf("[%2d]%c %-5d: %s (%s): %s\n", temp->number, def,
				temp->pid, temp->command, temp->raw_data, temp->state);
			temp->print = false;
		}
		delete = (temp->remove) ? temp : NULL;
		temp = temp->next;
		if (delete)
			st_chl_delete(delete->pid);
	}
}

/*
**	Loop runnner
**	This function runs main application loop, until lr_get_input returns false.
*/

void		lr_loop_run(void)
{
	char			*input;
	t_command		*head;
	bool			valid_line;

	head = NULL;
	input = NULL;
	valid_line = false;
	pb_print_prompt();
	while (lr_get_input(&input))
	{
		valid_line = lp_s0_replace_history(&input);
		if (input && input[0] != '\0')
			st_hst_push_back(input);
		if (valid_line && lp_process_line(&head, input))
			cp_process_commands(head);
		st_cmd_delete_list(&head);
		ft_strdel(&input);
		lr_loop_print_chl_messages();
		pb_print_prompt();
	}
	ft_strdel(&input);
	ft_putendl("exiting...");
}
